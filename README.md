# Responsive CSS with Sass and Compass Tutorial #

Project from Ray Villalobo's **[Responsive CSS with Sass and Compass course](http://www.lynda.com/CSS-tutorials/Responsive-CSS-Sass-Compass/140777-2.html)** on **Lynda.com**. Teaches the following:

* Sass
    * Compass
    * Susy
* Grunt.js Workflow

## Wide Layout ##
![large.jpg](https://bitbucket.org/repo/n6oGeM/images/671229054-large.jpg)

## Medium Layout ##
![medium.jpg](https://bitbucket.org/repo/n6oGeM/images/3671620695-medium.jpg)

## Small Layout ##
![small.jpg](https://bitbucket.org/repo/n6oGeM/images/23577235-small.jpg)